import torch
import torch.nn.functional as F
from torch.utils.checkpoint import checkpoint

from .utils import make_mlp

# 3rd party imports


class Transformer(torch.nn.Module):
    def __init__(self, hparams):
        super().__init__()
        """
        Initialise the Lightning Module that can scan over different embedding training regimes
        """
        self.hparams = hparams

        # Construct the MLP architecture
        scalar_channels = len(hparams["variables"]["scalar"]) if hparams["variables"]["scalar"] else 0
        charged_channels = len(hparams["variables"]["charged_pf"])
        neutral_channels = len(hparams["variables"]["neutral_pf"])

        self.charged_encoder_network = make_mlp(
            charged_channels,
            [hparams["hidden"]] * hparams["nb_layer"],
            hidden_activation=hparams["hidden_activation"],
            output_activation=hparams["output_activation"],
            layer_norm=True,
        )

        # Include charged/neutral feature?

        self.neutral_encoder_network = make_mlp(
            neutral_channels,
            [hparams["hidden"]] * hparams["nb_layer"],
            hidden_activation=hparams["hidden_activation"],
            output_activation=hparams["output_activation"],
            layer_norm=True,
        )

        self.scalar_encoder_network = make_mlp(
            scalar_channels,
            [hparams["hidden"]] * hparams["nb_layer"],
            hidden_activation=hparams["hidden_activation"],
            output_activation=hparams["output_activation"],
            layer_norm=True,
        )

        decoder_concat_multiple = (
            hparams["steps"] + 1 if hparams["concat_output"] else 1
        )

        out_channels = len(hparams["labels"]["scalar"])
        self.decoder_network = make_mlp(
            2*hparams["hidden"] * decoder_concat_multiple,
            [hparams["hidden"]] * hparams["nb_layer"] + [out_channels],
            hidden_activation=hparams["hidden_activation"],
            output_activation=None,
            layer_norm=False,
        )

        self.setup_transformer_layers()

    def output_aggregation(self, x):

        """
        Takes all nodes and sum aggregates them into a single output.
        """

        # Sum over the nodes
        x = torch.sum(x, dim=1)

        return x


    def forward(self, batch):
        """
        The forward pass of the transformer.

        Args:
            batch (dict): The batch of data to use for the forward pass.

        Returns:
            torch.Tensor: The output of the transformer.

        The batch structure is as follows:
            - batch[0] is a dictionary of inputs. Each key represents a different set of nodes.
            As a POC, we start with keys "charged_pf" and "neutral_pf" for charged and neutral PF candidates.
            Thus batch[0]["charged_pf"] is a tensor (batch_size, num_charged_pf, num_features) of charged PF candidates.
            And batch[0]["neutral_pf"] is a tensor (batch_size, num_neutral_pf, num_features) of neutral PF candidates.
            - batch[1] is a dictionary of masks. Each key represents a different set of nodes.
            Note that False in the mask dictionary, means that the node should NOT be masked, and is NOT a padding node.
            - batch[2] is a dictionary of labels. As a POC, we start with the event-level ("scalar") label only.
        """
        
        # Set up an empty list for optional concatenation purposes
        all_x = []

        # Get the inputs
        charged_pf = batch[0]["charged_pf"]
        neutral_pf = batch[0]["neutral_pf"]
        scalar = batch[0]["scalar"]

        # Get the masks
        charged_mask = batch[1]["charged_pf"]
        neutral_mask = batch[1]["neutral_pf"]


        # Encode the input
        x_charged = self.charged_encoder_network(charged_pf)
        x_neutral = self.neutral_encoder_network(neutral_pf)
        x_scalar = self.scalar_encoder_network(scalar)

        # Concat all the nodes together
        x = torch.cat([x_charged, x_neutral], dim=1)

        # Concat all the masks together
        mask = torch.cat([charged_mask, neutral_mask], dim=1)

        for i in range(self.hparams["steps"]):
            # Apply the attention layer
            if self.hparams.get("use_checkpointing"):
                x = checkpoint(
                    self.transformer_layers[i],
                    x,
                    src_key_padding_mask=mask,
                )
            else:
                x = self.transformer_layers[i](
                    x,
                    src_key_padding_mask=mask,
                )

            if self.hparams["concat_output"]:
                all_x.append(x)

        # Concatenate and decode the embeddings
        if self.hparams["concat_output"] and self.hparams["steps"] > 0:
            x = torch.cat(all_x, dim=1)

        x = self.output_aggregation(x)
        x = torch.cat([x, x_scalar], dim=1)

        return self.decoder_network(x)


    def setup_transformer_layers(self):
        
        self.transformer_layers = torch.nn.ModuleList(
                [
                    torch.nn.TransformerEncoderLayer(
                        d_model=self.hparams["hidden"],
                        nhead=self.hparams.get("nb_heads", 1),
                        dim_feedforward=self.hparams["hidden"],
                        dropout=self.hparams["dropout"],
                        activation=self.hparams["hidden_activation"].lower(),
                        batch_first=True,
                    )
                    for _ in range(self.hparams["steps"])
                ]
            )
