import torch.nn as nn
import torch
import h5py
import numpy as np
import torch
from numpy.lib.recfunctions import structured_to_unstructured as s2u
from torch.utils.data import Dataset, Sampler, DataLoader

def get_optimizers(parameters, hparams):
    optimizer = [
        torch.optim.AdamW(
            parameters,
            lr=(hparams["lr"]),
            betas=(0.9, 0.999),
            eps=1e-08,
            amsgrad=True,
        )
    ]

    if (
        "scheduler" not in hparams
        or hparams["scheduler"] is None
        or hparams["scheduler"] == "StepLR"
    ):
        scheduler = [
            {
                "scheduler": torch.optim.lr_scheduler.StepLR(
                    optimizer[0],
                    step_size=hparams["patience"],
                    gamma=hparams["factor"],
                ),
                "interval": "epoch",
                "frequency": 1,
            }
        ]
    elif hparams["scheduler"] == "ReduceLROnPlateau":
        scheduler = [
            {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(
                    optimizer[0],
                    mode="min",
                    factor=hparams["factor"],
                    patience=hparams["patience"],
                    verbose=True,
                ),
                "interval": "epoch",
                "frequency": 1,
            }
        ]
    elif hparams["scheduler"] == "CosineAnnealingWarmRestarts":
        scheduler = [
            {
                "scheduler": torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
                    optimizer[0],
                    T_0=hparams["patience"],
                    T_mult=2,
                    eta_min=1e-8,
                    last_epoch=-1,
                ),
                "interval": "epoch",
                "frequency": 1,
            }
        ]
    else:
        raise ValueError(f"Unknown scheduler: {hparams['scheduler']}")

    return optimizer, scheduler

class RandomBatchSampler(Sampler):
    def __init__(
        self,
        dataset: torch.utils.data.Dataset,
        batch_size: int,
        shuffle: bool = False,
        drop_last: bool = False,
    ):
        """Batch sampler for an h5 dataset.

        The batch sampler performs weak shuffling. Jets are batched first,
        and then batches are shuffled.

        Parameters
        ----------
        dataset : torch.data.Dataset
            Input dataset
        batch_size : int
            Number of events to batch
        shuffle : bool
            Shuffle the batches
        drop_last : bool
            Drop the last incomplete batch (if present)
        """
        self.batch_size = batch_size
        self.dataset_length = len(dataset)
        self.n_batches = self.dataset_length / self.batch_size
        self.nonzero_last_batch = int(self.n_batches) < self.n_batches
        self.drop_last = drop_last
        self.shuffle = shuffle

    def __len__(self):
        return int(self.n_batches) + int(not self.drop_last and self.nonzero_last_batch)

    def __iter__(self):
        if self.shuffle:
            self.batch_ids = torch.randperm(int(self.n_batches))
        else:
            self.batch_ids = torch.arange(int(self.n_batches))
        # yield full batches from the dataset
        for batch_id in self.batch_ids:
            start, stop = batch_id * self.batch_size, (batch_id + 1) * self.batch_size
            yield np.s_[int(start) : int(stop)]

        # in case the batch size is not a perfect multiple of the number of samples,
        # yield the remaining samples
        if not self.drop_last and self.nonzero_last_batch:
            start, stop = int(self.n_batches) * self.batch_size, self.dataset_length
            yield np.s_[int(start) : int(stop)]


class PFlowDataset(Dataset):
    def __init__(
        self,
        filename: str,
        num: int = -1,
        variables: dict = None,
        labels: dict = None,        
    ):
        """A map-style dataset for loading jets from a structured array file.

        Parameters
        ----------
        filename : str
            Input h5 filepath containing structured arrays
        num : int
            Number of events to load, -1 for all events in the file
        variables: dict
            A dictionary containing lists of variables
        labels: dict
            A dictionary containing lists of labels
        """
        super().__init__()

        # check labels have been configured
        self.filename = filename
        self.file = h5py.File(self.filename, "r")

        # set the variables to load
        self.variables = variables

        # set the labels to load
        self.labels = labels

        # if variables none, then set it to be a dictionary, with each key a list of the keys in that hdf5 dataset
        if self.variables is None:
            self.variables = {key: list(self.file[key].keys()) for key in self.file.keys()}

        # set the number of events to load
        self.num = self.get_num(num)


    def __len__(self):
        return int(self.num)

    def __getitem__(self, jet_idx):
        """Return on sample or batch from the dataset.

        Parameters
        ----------
        jet_idx
            A numpy slice corresponding to a batch of jets.

        Returns
        -------
        tuple
            Dict of tensor for each of the inputs, masks, and labels.
            Each tensor will contain a batch of samples.
        """
        inputs = {}
        labels = {}
        masks = {}

        for input_type, input_features in self.variables.items():
            # Create a NumPy array to hold the data
            batch_size = jet_idx.stop - jet_idx.start
            batch = np.empty((batch_size,) + self.file[input_type].shape[1:], dtype=self.file[input_type].dtype)

            # Read data from 'self.file[input_type]' directly into 'batch'
            self.file[input_type].read_direct(batch, np.s_[jet_idx.start : jet_idx.stop])

            flat_array = s2u(batch[input_features], dtype=np.float32).copy()
            inputs[input_type] = torch.from_numpy(flat_array)

            # apply the input padding mask
            if "valid" in batch.dtype.names:
                masks[input_type] = ~torch.from_numpy(batch["valid"])
                if input_type not in ["scalar"]:
                    inputs[input_type][masks[input_type]] = 0

            # check inputs are finite
            if not torch.isfinite(inputs[input_type]).all():
                raise ValueError(f"Non-finite inputs for '{input_type}' in {self.filename}.")

            # process labels for this input type
            if input_type in self.labels:
                labels[input_type] = {}
                for label in self.labels[input_type]:
                    dtype = torch.long if np.issubdtype(batch[label].dtype, np.integer) else None
                    labels[input_type][label] = torch.as_tensor(batch[label].copy(), dtype=dtype)

        return inputs, masks, labels

    def get_num(self, num_requested: int):
        
        num_available = len(self.file["scalar"])

        # not enough jets
        if num_requested > num_available:
            raise ValueError(
                f"Requested {num_requested:,} jets, but only {num_available:,} are"
                f" available in the file {self.filename}."
            )

        # use all jets
        if num_requested < 0:
            return num_available

        # use requested jets
        return num_requested