import h5py
import numpy as np
import torch
from numpy.lib.recfunctions import structured_to_unstructured as s2u
from torch.utils.data import Dataset, Sampler, DataLoader

import sys
import lightning as pl
import warnings
import torch.nn.functional as F

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

from .utils import get_optimizers, PFlowDataset, RandomBatchSampler
from .networks.transformer import Transformer


class JetRegression(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        """
        Initialise the Lightning Module that can scan over different GNN training regimes
        """

        self.save_hyperparameters(hparams)

        # Assign hyperparameters
        self.trainset, self.valset, self.testset = None, None, None
        self.dataset_class = PFlowDataset
        self.network = Transformer(hparams)

    def setup(self, stage="fit"):
        """
        The setup logic of the stage.
        1. Setup the data for training, validation and testing.
        2. Run tests to ensure data is of the right format and loaded correctly.
        3. Construct the truth and weighting labels for the model training
        """
        input_dir = "input_dir"
        self.load_data(stage, self.hparams[input_dir])
        torch.set_float32_matmul_precision("medium" if stage == "fit" else "high")
        
        try:
            print("Defining figures of merit")
            self.logger.experiment.define_metric("val_loss", summary="min")
        except Exception:
            warnings.warn(
                "Failed to define figures of merit, due to logger unavailable"
            )

    def load_data(self, stage, input_dir):
        """
        Load in the data for training, validation and testing.
        """

        for data_name, data_num in zip(
            ["trainset", "valset", "testset"], self.hparams["data_split"]
        ):
            if data_num > 0:
                dataset = self.dataset_class(
                    filename=f"{input_dir}/{data_name}.h5",
                    num=data_num,
                    variables=self.hparams["variables"],
                    labels=self.hparams["labels"],
                )
                setattr(self, data_name, dataset)

    def get_dataloader(self, stage: str, dataset: PFlowDataset, shuffle: bool):
        drop_last = stage == "fit"
        return DataLoader(
            dataset=dataset,
            batch_size=None,
            collate_fn=None,
            sampler=RandomBatchSampler(dataset, self.hparams.get("batch_size", 10), shuffle, drop_last=drop_last),
            num_workers=self.hparams.get("num_workers", 1),
            shuffle=False,
            pin_memory=self.hparams.get("pin_memory", True),
        )

    def train_dataloader(self):
        if self.trainset is None:
            return None
        return self.get_dataloader("fit", self.trainset, shuffle=True)

    def val_dataloader(self):
        if self.valset is None:
            return None
        return self.get_dataloader("val", self.valset, shuffle=False)

    def test_dataloader(self):
        if self.testset is None:
            return None
        return self.get_dataloader("test", self.testset, shuffle=False)
    
    def predict_dataloader(self):
        return [self.train_dataloader(), self.val_dataloader(), self.test_dataloader()]

    def configure_optimizers(self):
        optimizer, scheduler = get_optimizers(self.parameters(), self.hparams)
        return optimizer, scheduler

    def forward(self, batch):
        return self.network(batch)

    def training_step(self, batch, batch_idx):
        output = self(batch)
        loss, truth = self.loss_function(output, batch)

        self.log(
            "train_loss",
            loss,
            on_step=False,
            on_epoch=True,
            batch_size=1,
            sync_dist=True,
        )

        return loss

    def loss_function(self, output, batch):
        """
        An MSE loss that targets the batch[2] labels, using the hparams[labels]
        """

        truth = batch[2]["scalar"]["j_l_truth_pt"]

        return F.mse_loss(output.squeeze().float(), truth.squeeze().float()), truth

    def shared_evaluation(self, batch, batch_idx):
        output = self(batch)
        loss, truth = self.loss_function(output, batch)
        
        current_lr = self.optimizers().param_groups[0]['lr']

        self.log_dict(
            {
                "learning_rate": current_lr,
                "val_loss": loss,
            },
            on_step=False,
            on_epoch=True,
            sync_dist=True,
        )

        return {
            "loss": loss.detach(),
            "truth": truth,
            "output": output.detach(),
            "batch": batch,
        }

    def validation_step(self, batch, batch_idx):
        output_dict = self.shared_evaluation(batch, batch_idx)


    def test_step(self, batch, batch_idx):
        return self.shared_evaluation(batch, batch_idx)

    def log_metrics(self, output, all_truth, target_truth, loss):
        preds = torch.sigmoid(output) > self.hparams["edge_cut"]

        # Positives
        edge_positive = preds.sum().float()

        # Signal true & signal tp
        target_true = target_truth.sum().float()
        target_true_positive = (target_truth.bool() & preds).sum().float()
        all_true_positive = (all_truth.bool() & preds).sum().float()

        # add torch.sigmoid(output).float() to convert to float in case training is done with 16-bit precision
        target_auc = roc_auc_score(
            target_truth.bool().cpu().detach(),
            torch.sigmoid(output).float().cpu().detach(),
        )
        true_and_fake_positive = (
            edge_positive - (preds & (~target_truth) & all_truth).sum().float()
        )

        # Eff, pur, auc
        target_eff = target_true_positive / target_true
        target_pur = target_true_positive / edge_positive
        total_pur = all_true_positive / edge_positive
        purity = target_true_positive / true_and_fake_positive
        current_lr = self.optimizers().param_groups[0]["lr"]

        self.log_dict(
            {
                "current_lr": current_lr,
                "eff": target_eff,
                "target_pur": target_pur,
                "total_pur": total_pur,
                "pur": purity,
                "auc": target_auc,
            },  # type: ignore
            sync_dist=True,
            batch_size=1,
            on_epoch=True,
            on_step=False,
        )

        return preds

    def on_train_start(self):
        self.trainer.strategy.optimizers = [
            self.trainer.lr_scheduler_configs[0].scheduler.optimizer
        ]

    def on_before_optimizer_step(self, optimizer, *args, **kwargs):
        # warm up lr
        if (self.hparams["warmup"] is not None) and (
            self.trainer.current_epoch < self.hparams["warmup"]
        ):
            lr_scale = min(
                1.0, float(self.trainer.current_epoch + 1) / self.hparams["warmup"]
            )
            for pg in optimizer.param_groups:
                pg["lr"] = lr_scale * self.hparams["lr"]

        # after reaching minimum learning rate, stop LR decay
        for pg in optimizer.param_groups:
            pg["lr"] = max(pg["lr"], self.hparams.get("min_lr", 0))

