#!/bin/bash

#SBATCH -A m3443 -q regular
#SBATCH -C gpu
#SBATCH -t 60:00
#SBATCH -n 1
#SBATCH --ntasks-per-node=1
#SBATCH --gpus-per-task=1
#SBATCH -c 32
#SBATCH -o logs/%x-%j.out
#SBATCH -J GNN-train
#SBATCH --gpu-bind=none

# This is a generic script for submitting training jobs to Cori-GPU.
# You need to supply the config file with this script.

# Setup
mkdir -p logs
eval "$(conda shell.bash hook)"

conda activate microchallenges

export SLURM_CPU_BIND="cores"
echo -e "\nStarting sweeps\n"

# Single GPU training
srun -u mc_train $@